
aut = {
      'url' : "https://www.amazon.com/clouddrive",
      'book' : "C# For Dummies",
      'timeout' : "10",
      }

initialPage = {
        'PageTitle'         : "Amazon Cloud page",
        'SearchInput'       : "Search input field",
        'SearchButton'      : "Search button field",
        'lblSigninToolTip'  : "xpath=//*[@id='nav-signin-tooltip']",
        'txtSearchArea'     : "xpath=//*[@id='twotabsearchtextbox']",
        'btSearch'          : "xpath=//*[@class='nav-search-submit nav-sprite']",
    }

listPage = {
        'PageTitle'         : "Search article list page",
        'ShowResultsTitle'  : "Show results title description",
        'SortPriceCB'       : "Sort price object",
        'LowerPriceOptCB'   : "Lower price option",
        'SearchListResultLB' : "Search list result",
        'txtShowResultsTitle' : "xpath=//*[@id='leftNavContainer']//*[@class='a-size-medium a-spacing-base a-spacing-top-small a-color-tertiary a-text-normal']",
        'lstFirstResults'   : "xpath=//*[@id='s-results-list-atf']",
        'cbSearchForm'      : "xpath=//*[@id='searchSortForm']",
        'cbLowerPriceOpt'   : "xpath=//*[@id='sort']/option[2]",
        'lstResults'        : "xpath=//*[@id='s-results-list-atf']//li",
        'lbBookInTheList'   : "xpath=//ul[@id='s-results-list-atf']//li//h2[text()='C# For Dummies']",
    }

articlePage = {
    'PageTitle'             : "Article page",
    'PaperbackType'         : "Paperback article option",
    'AddToCartButton'       : "Add to cart button",
    'lblDeliveryArticleTypes' : "xpath=//*[@id='mediaTabs_tabSetContainer']",
    'lblPaperback'          : "xpath=//*[contains(@class,'a-size-large mediaTab_title') and contains(text(),'Paperback')]",
    'btAddToCart'           : "xpath=//*[@id='add-to-cart-button-ubb']",
    'lblAddedClass'         : "xpath=//*[@id='huc-v2-order-row-icon']",
    }

page1 = {
        'batatas' : 'fritas',
        }
