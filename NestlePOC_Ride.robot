*** Settings ***
Resource          Keywords.txt

*** Test Cases ***
TC_01_SearchForTheCheapestBook
    Given I navigate to the "Amazon Drive" site
    Capture Page Screenshot
    And I Search For My Book
    Capture Page Screenshot
    When I Select The Cheaper Book From The List
    Capture Page Screenshot
    And I Select The Book Type
    Capture Page Screenshot
    And I Add My Book To The Shopping Cart
    Capture Page Screenshot
    Then my book must be added to the shipping cart
    Capture Page Screenshot
    [Teardown]    Finish Test
