

1. Introduction


The idea of the current automated test is to search for a cheaper article in the 'Amazon Cloud' website and
insert it into the shopping cart.

In terms of inputs, it will be searched for the "C# For Dummies" book.

The test will only passed if the "C# for Dummies" book is correctly inserted into the 'Amazon Cloud'
shopping cart, validating the successfuly message retrieved by the website.


2. Pre-Requirements


The current automated test will developped in python, using the following frameworks:
- Robot Framework				
	--> IDE to develop the AT source code
- Selenium.WebDriver		
	--> Manipulating the Selenium WebDriver during the automated tests
- Selenium.Chrome.WebDriver	
	--> Creating a WebDriver to run the automated tests on the Chrome browser


3. TestCase flow


The current automated test will be runned over the following steps:

Scenario: TC_01 - Search for the cheapest book
	Given I navigate to the Amazon Drive site
	And I search for my book
	When I select the cheaper book from the list
	And I select the book type
	And I add my book to the shopping cart
	Then my book must be added to the shipping cart


4. Test execution


Before clonning the project from the bitbucket, please confirm that you already has the following
tools:
a. Google Chrome
	--> Web Browser to run the current automated test
b. Python 2.7
	--> Python source code libraries
c. wxPython
	--> Wrapper for the python cross-platform 
d. Install the 'robotframework' and 'robotframework-selenium2library'

After clonning the git project, it must be performed the following steps:
a. Navigate to the python scripts folder
	--> Usually, the directory is "C:\Python 27\Scripts"
b. Run the 'robot' instruction


5. Test output


5.1 Without using VS

The test output will be visible after running the robot instruction and a log file is stored in the following
directory:
	--> "C:\Python 27\Scripts\log.html"